# web3-201802-thiagocosta

# Trabalho Final Web 3

Uma empresa possui um almoxarifado, onde são armazenados todos os produtos a serem distribuidos para todos os setores. O responsável pelo gerenciamento do almoxarifado, percebeu que alguns setores estão recebendo quantidades excessivas de determinados produtos.A fim de controlar o uso desses recursos ele resolveu desenvolver um sistema, para que os funcionários realizem os pedidos com a quantidade de produtos para um determinado setor.

Funcionalidades do Administrador
* Cadastrar setores
* Manter produtos
* visualizar histórico
* Visualizar pedidos 
* Confirmar/Recusar um pedido

Funcionalidades dos funcionários
* Registrar-se
* Manter pedidos de produtos

### Prerequisites

Computador com Ruby e Rails instalados. 

### Installing

Para instalar o Ruby em computadores com S.O. GNU/Linux utilize o comando : 

```
sudo apt-get install ruby
```


## Authors

* **Thiago Costa** - *Initial work* - [thiagocosta](https://gitlab.com/th1ag0/web3-201802-thiagocosta/)

