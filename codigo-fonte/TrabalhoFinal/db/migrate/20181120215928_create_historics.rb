class CreateHistorics < ActiveRecord::Migration[5.1]
  def change
    create_table :historics do |t|
      t.date :date
      t.integer :quantity
      t.string :historic_type
      t.references :product, foreign_key: true
      t.references :employee, foreign_key: true

      t.timestamps
    end
  end
end
