class CreateInputHistories < ActiveRecord::Migration[5.1]
  def change
    create_table :input_histories do |t|
      t.date :date
      t.integer :quantity
      t.references :employee, foreign_key: true
      t.references :product, foreign_key: true
      t.timestamps
    end
  end
end
