class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.integer :status
      t.integer :quantity
      t.references :employee, foreign_key: true
      t.references :product, foreign_key: true
      t.timestamps
    end
  end
end