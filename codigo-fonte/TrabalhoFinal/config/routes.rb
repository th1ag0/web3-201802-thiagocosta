Rails.application.routes.draw do

  
  resources :orders
  
  devise_for :employees, 
              controllers: { registrations: 'employees/registrations' },
              path: '', path_names: { sign_in: 'login', sign_out: 'logout', sign_up: 'register' }
  
  get 'pedido/fazerPedido'

  resources :products
  resources :categories
  resources :historics
  # get 'historics', to: 'historics#index'
  # get 'historics/:id', to: 'historics#show', as: :historic
  get 'categories', to: 'categories#index'
  get 'categories/new', to: 'categories#new', as: :new_company
  get 'categories/:id', to: 'categories#show', as: :company
  post 'categories', to: 'categories#create'

  root 'home#index'


  resources :departments
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
