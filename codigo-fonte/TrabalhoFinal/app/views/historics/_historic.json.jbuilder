json.extract! historic, :id, :date, :quantity, :historic_type, :product_id, :employee_id, :created_at, :updated_at
json.url historic_url(historic, format: :json)
