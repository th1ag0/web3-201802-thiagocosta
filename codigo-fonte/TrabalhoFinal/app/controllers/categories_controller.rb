class CategoriesController < ApplicationController
  before_action :authenticate_employee!
  before_action :set_category, only: [:show, :edit, :update, :destroy]

  # GET /categories
  # GET /categories.json
  def index
    
    if !current_employee.admin?
      render json: {status: "error", message: "Você não têm permissão Para acessar esta Página"}
    end 
      @categories = Category.all         
    # employees = Employee.all
    # for value in employees do
    #   if value.department.name == "Gerente de Estoque"
    #     @gerente = true
    #   end
    # end
  end

  # GET /categories/1
  # GET /categories/1.json
  def show
    if !current_employee.admin?
      render json: {status: "error", message: "Você não têm permissão Para acessar esta Página"}
    end       
  end

  # GET /categories/new
  def new
    if !current_employee.admin?
      render json: {status: "error", message: "Você não têm permissão Para acessar esta Página"}
    end 
      @category = Category.new            
  end

  # GET /categories/1/edit
  def edit
    if !current_employee.admin?
      render json: {status: "error", message: "Você não têm permissão Para acessar esta Página"}
    end      
  end

  # POST /categories
  # POST /categories.json
  def create
    if !current_employee.admin?
      render json: {status: "error", message: "Você não têm permissão Para acessar esta Página"}
    end     
    @category = Category.new(category_params)

    respond_to do |format|
      if @category.save
        flash[:sucess] = "Categoria Criada."
        format.html { redirect_to @category}
        format.json { render :show, status: :created, location: @category }
      else
        flash[:error] = "Problema ao criar Categoria."
        format.html { render :new }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /categories/1
  # PATCH/PUT /categories/1.json
  def update
    if !current_employee.admin?
      render json: {status: "error", message: "Você não têm permissão Para acessar esta Página"}
    end     
    respond_to do |format|
      if @category.update(category_params)
        flash[:sucess] = "Categoria atualizada."
        format.html { redirect_to @category}
        format.json { render :show, status: :ok, location: @category }
      else
        flash[:error] = "Não foi possível atualizar a categoria."
        format.html { render :edit }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categories/1
  # DELETE /categories/1.json
  def destroy
    if !current_employee.admin?
      render json: {status: "error", message: "Você não têm permissão Para acessar esta Página"}
    end     
    @category.destroy
    respond_to do |format|
      format.html { redirect_to categories_url, notice: 'Category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_params
      params.require(:category).permit(:name)
    end
end
