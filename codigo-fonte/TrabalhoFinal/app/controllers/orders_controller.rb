class OrdersController < ApplicationController
  before_action :authenticate_employee!
  before_action :set_order, only: [:show, :edit, :update, :destroy]

  # GET /orders
  # GET /orders.json
  def index

    @orders = Order.where("status = 0 OR status = 3").where("employee_id = #{current_employee.id}")
    
    if current_employee.admin?
      @orders = Order.where("status = 3")
      render "indexAdm"
    end

  end

  # GET /orders/1
  # GET /orders/1.json
  def show
    if current_employee.admin?
      render json: {status: "error", message: "Você não têm permissão Para acessar esta Página"}
    end 
  end

  # GET /orders/new
  def new
    if !current_employee.admin?
      @order = Order.new
    else
      render json: {status: "error", message: "Você não têm permissão Para acessar esta Página"}
    end         
  end

  # GET /orders/1/edit
  def edit
    if current_employee.admin?
      render json: {status: "error", message: "Você não têm permissão Para acessar esta Página"}
    end 
  end

  # POST /orders
  # POST /orders.json
  def create
  
    @order = Order.new(order_params)

    respond_to do |format|
      if @order.save
        flash[:sucess] = "O pedido realizado com sucesso."
        format.html { redirect_to @order}
        format.json { render :show, status: :created, location: @order }
      else
        flash[:sucess] = "Falha ao realizar pedido."
        format.html { render :new }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orders/1
  # PATCH/PUT /orders/1.json

  def update
    respond_to do |format|

      status = Integer(order_params[:status])
      id = @order.product.id
      produto = Product.find(id)
      quantidadeAtual = produto.quantity
      quantidadePedido = @order.quantity
      case status
        when 1

          if quantidadePedido < quantidadeAtual

            total = quantidadeAtual - quantidadePedido
            if atualizar_produto id, total
              time = Time.now
              usuario = Integer(current_employee.department.id)
              @historic = Historic.new(date: "2018-10-12", quantity: quantidadePedido, historic_type: "SAIDA" , product_id: id, employee_id: usuario)
              if @historic.save
                puts "saida Registrada#{time}"
              end
            end
            puts "status#{status}"
            puts "ID: #{id}"
            puts "Produto: #{quantidadeAtual}"
            puts "Pedido: #{quantidadePedido}"
            puts "Pedido: #{total}"
            if @order.update(order_params)
              flash[:sucess] = "O pedido foi atualizado com sucesso."
              format.html { redirect_to action: :index}
              format.json { render :show, status: :ok, location: @order }
            else

              format.html { render :show }
              format.json { render json: @order.errors, status: :unprocessable_entity }
            end
          else
            flash[:error] = "A quantidade solicitada excede a quantidade disponível."
            format.html { redirect_to action: :index}
          end
        else

          if @order.update(order_params)
            if status == 0
              flash[:sucess] = "O pedido foi recusado."
            elsif status == 3
              flash[:sucess] = "O pedido foi atualizado com sucesso."
            end
            
            format.html { redirect_to action: :index}
            format.json { render :show, status: :ok, location: @order }
          else
            flash[:error] = "O pedido não foi alterado."
            format.html { render :show }
            format.json { render json: @order.errors, status: :unprocessable_entity }
          end
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy  
    @order.destroy
    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Order was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  def atualizar_produto id, total
    Product.update(id, :quantity => total)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:order).permit(:status, :quantity, :employee_id, :product_id)
    end
end
