class HistoricsController < ApplicationController
  before_action :authenticate_employee!
  before_action :set_historic, only: [:show, :edit, :update, :destroy]

  # GET /historics
  # GET /historics.json
  def index
    if !current_employee.admin?
      render json: {status: "error", message: "Você não têm permissão Para acessar esta Página"}
    end     
    if current_employee.admin?
      @historics = Historic.all
      @entradas = 200
      @saidas = 500            
    else
      render json: {status: "error", code: 3000, message: "Você não têm permissão Para acessar esta Página"}
    end
  end

  # GET /historics/1
  # GET /historics/1.json
  def show
    if !current_employee.admin?
      render json: {status: "error", message: "Você não têm permissão Para acessar esta Página"}
    end     
    id = params[:id]
    @produto = Product.find(id)
    @entradas = buscaEntradas id
    @saidas = buscaSaidas id
  end

  private
  def buscaEntradas id
    id = id
    quantidades = Historic.where(product_id: id,historic_type: 'ENTRADA').select("quantity")
    entradas = 0
    quantidades.each do |quantidade|
      entradas += quantidade.quantity
    end
    return entradas
  end

  private
  def buscaSaidas id
    id = id
    quantidades = Historic.where(product_id: id,historic_type: 'SAIDA').select("quantity")
    saidas = 0
    quantidades.each do |quantidade|
      saidas += quantidade.quantity
    end
    return saidas
  end
  # GET /historics/new
  def new
    if !current_employee.admin?
      render json: {status: "error", message: "Você não têm permissão Para acessar esta Página"}
    end     
    @historic = Historic.new
  end

  # GET /historics/1/edit
  def edit
    if !current_employee.admin?
      render json: {status: "error", message: "Você não têm permissão Para acessar esta Página"}
    end     
  end

  # POST /historics
  # POST /historics.json
  def create
    if !current_employee.admin?
      render json: {status: "error", message: "Você não têm permissão Para acessar esta Página"}
    end     
    @historic = Historic.new(historic_params)

    respond_to do |format|
      if @historic.save
        format.html { redirect_to @historic, notice: 'Historic was successfully created.' }
        format.json { render :show, status: :created, location: @historic }
      else
        format.html { render :new }
        format.json { render json: @historic.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /historics/1
  # PATCH/PUT /historics/1.json
  def update
    if !current_employee.admin?
      render json: {status: "error", message: "Você não têm permissão Para acessar esta Página"}
    end     
    respond_to do |format|
      if @historic.update(historic_params)
        format.html { redirect_to @historic, notice: 'Historic was successfully updated.' }
        format.json { render :show, status: :ok, location: @historic }
      else
        format.html { render :edit }
        format.json { render json: @historic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /historics/1
  # DELETE /historics/1.json
  def destroy
    if !current_employee.admin?
      render json: {status: "error", message: "Você não têm permissão Para acessar esta Página"}
    end     
    @historic.destroy
    respond_to do |format|
      format.html { redirect_to historics_url, notice: 'Historic was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_historic
      @historic = Historic.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def historic_params
      params.require(:historic).permit(:date, :quantity, :historic_type, :product_id, :employee_id)
    end
end
