class ProductsController < ApplicationController
  before_action :authenticate_employee!
  before_action :set_product, only: [:show, :edit, :update, :destroy]

  # GET /products
  # GET /products.json
  def index
    @products = Product.all
    if current_employee.admin?
    else
      render json: {status: "error", code: 3000, message: "Can't find purchases without start and end date"}
    end    
  end

  # GET /products/1
  # GET /products/1.json
  def show
  end

  # GET /products/new
  def new
    @product = Product.new

  end

  # GET /products/1/edit
  def edit
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        date = Time.now
        date.strftime("%Y %m %d")
        id = Integer(@product.id)
        quantidade = Integer(@product.quantity)
        usuario = Integer(current_employee.id)
        @historic = Historic.new(date: date, quantity: quantidade, historic_type: "ENTRADA" , product_id: id, employee_id: usuario)
        if @historic.save
          puts "conseguiiiiiiiii#{date.strftime("%Y %m %d")}"
        end
        flash[:sucess] = "Produto cadastrado com sucesso."
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        flash[:sucess] = "Falha ao cadastrar Produto."
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    respond_to do |format|
      id = Integer(@product.id)
      quantidadeAnterior = Integer(@product.quantity)
      usuario = Integer(current_employee.id)
      if @product.update(product_params)
        time = Time.now
        time.strftime("%Y %m %d")
        quantidadeNova = Integer(@product.quantity)
        quantidade = quantidadeNova - quantidadeAnterior
        if quantidade > 0
          his_type = "ENTRADA"
        else
          quantidade *= -1
          his_type = "SAIDA"
        end
        
        @historic = Historic.new(date: "2018-10-12", quantity: quantidade, historic_type: his_type , product_id: id, employee_id: usuario)
        if quantidade != 0
          if @historic.save
            puts "ENTRADA#{time}"
          end
        end
        puts "ID:#{id}"
        puts "Quantidade Anterior:#{quantidadeAnterior}"
        puts "Quantidade Nova:#{quantidadeNova}"
        puts "Resultado: #{quantidade}"

        flash[:sucess] = "Produto atualizado com sucesso."
        format.html { redirect_to @product}
        format.json { render :show, status: :ok, location: @product }
      else
        flash[:error] = "Falha ao atualizar produto."
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:name, :description, :quantity, :category_id)
    end
end
